from setuptools import find_packages, setup

setup(
    name='devops_toolkit',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        # List your package dependencies here
        # e.g., 'requests>=2.25.1',
    ],
    author='Your Name',
    author_email='your.email@example.com',
    description='A short description of your package',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    url='https://github.com/yourusername/your_package_name',
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
)
