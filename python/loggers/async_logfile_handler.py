"""Asynchronous File Logging Handler.

This module provides a custom logging handler, AsyncFileHandler, which extends
Python's standard logging.FileHandler to support asynchronous log writing. This
handler is designed for applications where non-blocking I/O is crucial, such as in
asynchronous web applications and other I/O-bound contexts.

The AsyncFileHandler ensures that logging operations do not block the main execution
flow by offloading the I/O operations to separate asynchronous tasks. This is particularly
beneficial in scenarios where frequent and fast log writes are necessary, and any delay
caused by synchronous file writing is undesirable.

In addition to its primary functionality, the handler includes a mechanism to manage and
clean up completed asynchronous tasks, helping to maintain efficient use of resources.

Copyright (c) 2024 Neil Schneider

Usage:
    To use the AsyncFileHandler, simply instantiate it and add it to a logger as you would
    with a standard FileHandler:

    ```
    import logging
    from async_file_handler import AsyncFileHandler

    # Configure logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Create and add AsyncFileHandler
    async_handler = AsyncFileHandler('path/to/logfile.log')
    logger.addHandler(async_handler)

    # Log messages
    logger.debug('This is a debug message.')
    ```

Note:
    - The AsyncFileHandler requires the asyncio module, available in Python 3.5 and later.
    - While the handler writes logs asynchronously, it's important to ensure proper closure
      of the handler to release resources. This is typically handled via the logging module's
      shutdown mechanism.

"""

import asyncio
import logging
from pathlib import Path

error_logger = logging.getLogger('error_logger')
error_logger.setLevel(logging.ERROR)
error_stream_handler = logging.StreamHandler()  # or use FileHandler for a file
error_stream_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
error_logger.addHandler(error_stream_handler)


class AsyncFileHandler(logging.FileHandler):
    """Asynchronous file handler for logging.

    Extends the standard FileHandler to enable asynchronous log writing.
    It maintains a list of tasks for log writing operations and periodically cleans them up
    to avoid memory overflow.

    Attributes:
        _tasks (List[asyncio.Task]): A list to store references to asyncio tasks for log writing.
        _cleanup_interval (float): Interval in seconds for cleaning up completed tasks.
        _cleanup_task (asyncio.Task): A task that runs periodically to clean up completed tasks.
    """

    def __init__(
        self,
        filename: str,
        mode: str = 'a',
        encoding: str | None = None,
    ) -> None:
        """Initialize the AsyncFileHandler.

        This constructor initializes the AsyncFileHandler with a specific file name, mode,
        encoding, and delay setting. It extends the functionality of the standard FileHandler
        by setting up an asynchronous task management system.

        Args:
            filename (str): The name of the file to which log messages will be written.
            mode (str): The mode in which the file is opened. Default is 'a' for append.
            encoding (Optional[str]): The encoding used for the log file. Default is None.
        """
        super().__init__(filename, mode, encoding)
        self._tasks: list[asyncio.Task] = []
        self._cleanup_interval: float = 1.0  # Interval in seconds for cleanup
        self._cleanup_task: asyncio.Task = asyncio.create_task(self._cleanup_tasks())

    def emit(self, record: logging.LogRecord) -> None:
        """Emit a log record asynchronously.

        Overrides the method from FileHandler to handle the log record asynchronously.
        Creates a task for writing the log and adds it to the task list.

        Args:
            record (logging.LogRecord): The log record to be emitted.
        """
        try:
            log_entry: str = self.format(record)
            task: asyncio.Task = asyncio.create_task(self._write_log(log_entry))
            self._tasks.append(task)
        except Exception:
            error_logger.exception('Error with async log record writing: %s', record)

    async def _write_log(self, message: str) -> None:
        """Asynchronously writes a log message to the file.

        This method uses asyncio to perform file writing in a non-blocking manner.

        Args:
            message (str): The log message to be written.
        """
        loop = asyncio.get_event_loop()
        try:
            await loop.run_in_executor(None, self._sync_write, message)
        except Exception:
            # Handle exceptions that occur during asynchronous logging
            error_logger.exception('Failed to write log asynchronously.')

    def _sync_write(self, message: str) -> None:
        """Synchronously writes a log message to the file in a separate thread.

        This method is used to perform the actual file writing operation,
        called asynchronously from _write_log.

        Args:
            message (str): The log message to be written.
        """
        logpath = Path(self.baseFilename)
        try:
            with logpath.open('a', encoding=self.encoding, errors=self.errors) as file:
                file.write(message + self.terminator)
        except Exception:
            error_logger.exception('Error writing log message to file.')

    async def _cleanup_tasks(self) -> None:
        """Periodically clean up completed logging tasks.

        This method runs in an endless loop, sleeping for a specified interval.
        During each cycle, it removes completed tasks from the task list.

        Note: The loop is broken when the cleanup task is cancelled during the handler's closure.
        """
        try:
            while True:
                self._tasks = [t for t in self._tasks if not t.done()]
                await asyncio.sleep(self._cleanup_interval)
        except asyncio.CancelledError:
            pass  # Handle cancellation of the cleanup task
        except Exception:
            error_logger.exception('Error during cleanup of log tasks.')

    def close(self) -> None:
        """Close the handler and clean up resources.

        This method ensures that the cleanup task is cancelled when the handler is closed,
        and any remaining resources are properly released.

        Note: It's important to ensure that this method is called to avoid resource leaks.
        """
        try:
            self._cleanup_task.cancel()
            asyncio.get_event_loop().run_until_complete(self._cleanup_task)
        except Exception:
            error_logger.exception('Error closing AsyncFileHandler.')
        finally:
            super().close()
