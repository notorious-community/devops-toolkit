"""Enhanced Logger Module.

This module provides an advanced logging utility to automatically redact sensitive data from logs.
It covers patterns and environment variables ending in specific suffixes. The logger supports sync
and async logging to files and the console, with configurable levels and formats. It includes a
decorator for simple logging of function activities and exceptions.

Features:
- Auto-redacts sensitive patterns and chosen environment variables.
- Customizable suffix for identifying sensitive environment variables.
- Clear type hints for better understanding and IDE integration.
- Robust error handling for regex and environment variable processing.
- Configurable logging for different development stages.

Usage:
Import the module, configure the logger as desired, and either use the decorator for function
logging or directly call the logging methods for tailored log messages.

Copyright (c) 2024 Neil Schneider
"""

import logging
import os
import re
import time
from collections.abc import Callable
from typing import ParamSpec, TypeVar

from python.loggers.async_logfile_handler import AsyncFileHandler

# These are used in the trace_function wrapper
T_log = TypeVar('T_log')
P_log = ParamSpec('P_log')

# Set up logging for this modules since it can't log itself.
error_logger = logging.getLogger('error_logger')
error_logger.setLevel(logging.ERROR)
error_stream_handler = logging.StreamHandler()
error_stream_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
error_logger.addHandler(error_stream_handler)


class RedactionFormatter(logging.Formatter):
    """Formats log messages by redacting sensitive data.

    Extends standard logging.Formatter to identify and mask sensitive information
    in log messages, including specific environment variables.

    Attributes:
        base_formatter (logging.Formatter): Underlying formatter for initial formatting.
        sensitive_suffix (str): Suffix for environment variables with sensitive data.
        redaction_patterns (List[str]): Regex patterns for sensitive data.
    """

    REDACTED: str = '[REDACTED]'

    def __init__(
        self,
        base_formatter: logging.Formatter,
        sensitive_suffix: str = '_SECRET',
    ) -> None:
        """Initialize RedactionFormatter.

        Args:
            base_formatter (logging.Formatter): Underlying formatter for initial formatting.
            sensitive_suffix (str): Suffix for environment variables with sensitive data.
        """
        super().__init__()
        self.base_formatter = base_formatter
        self.sensitive_suffix = sensitive_suffix
        self.redaction_patterns: list[str] = self._default_patterns()
        self._load_sensitive_env_vars()

    @staticmethod
    def _default_patterns() -> list[str]:
        """Sets default patterns for sensitive data."""
        return [
            r'\b\d{4}[-.]?\d{4}[-.]?\d{4}[-.]?\d{4}\b',  # Credit card numbers
            r'\b\d{3}-\d{2}-\d{4}\b',  # SSN
            r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b',  # Email addresses
        ]

    def _load_sensitive_env_vars(self) -> None:
        """Adds environment variables ending with sensitive_suffix to patterns."""
        try:
            sensitive_env_vars = {k: v for k, v in os.environ.items() if k.endswith(self.sensitive_suffix)}
            for value in sensitive_env_vars.values():
                pattern = re.escape(value)
                if pattern not in self.redaction_patterns:
                    self.redaction_patterns.append(pattern)
        except re.error:
            logging.exception('Failed to properly escape env variable values: %s')
        except Exception as e:
            logging.exception('Error loading sensitive environment variables: %s')
            raise RuntimeError from e

    def format(self, record: logging.LogRecord) -> str:
        """Formats the log record by redacting sensitive info before standard formatting."""
        message = self.base_formatter.format(record)

        combined_pattern = '|'.join(self.redaction_patterns)

        try:
            message = re.sub(combined_pattern, self.REDACTED, message)
        except re.error:
            logging.exception('Regex redaction error.')

        return message


class EnhancedLogging:
    """Facilitates enhanced logging with sensitive data redaction.

    Offers methods for setting up logging configurations, including file and console handlers,
    and a decorator for detailed function logging.

    Methods:
        configure_logger: Sets up global logger with file and console handlers.
        log_function: Decorator for detailed function execution logging.
    """

    @staticmethod
    def configure_logger(
        log_path: str = '.log/app.log',
        log_level: int = logging.INFO,
        env: str | None = None,
        sensitive_suffix: str = '_SECRET',
        *,  # This triggers all following parameters to be keyword-only
        force_reconfigure: bool = False,
    ) -> None:
        """Sets up the global logger with file/console handlers and redaction features.

        Parameters:
            log_path (str): Log file path.
            log_level (int): Logging level, e.g., logging.INFO.
            env (Optional[str]): Running environment ('development' or 'production').
            sensitive_suffix (str): Suffix for sensitive env vars.
        """
        env = env or os.getenv('LOG_ENV', 'production')
        logger = logging.getLogger()

        if not logger.handlers or force_reconfigure:
            logger.setLevel(log_level)
            logger.handlers.clear()

            standard_formatter = logging.Formatter(
                '%(asctime)s - %(levelname)s - %(name)s - %(message)s',
            )
            redaction_formatter = RedactionFormatter(standard_formatter, sensitive_suffix)

            file_handler = AsyncFileHandler(log_path)
            file_handler.setFormatter(redaction_formatter)
            logger.addHandler(file_handler)

            if env == 'development':
                console_handler = logging.StreamHandler()
                console_handler.setFormatter(redaction_formatter)
                logger.addHandler(console_handler)

            logger.info('Logger configured with redaction.')
        else:
            logger.info('Logger already configured.')

    # I am sorry I took the time to type annotation this. It is ugly.
    @staticmethod
    def trace_function(
        *,
        timeit: bool = False,
        log_level: str = 'INFO',
        show_args: bool = False,
    ) -> Callable[[Callable[P_log, T_log]], Callable[P_log, T_log]]:
        """Decorator factory for logging function activities and exceptions.

        Parameters:
            timeit (bool): If True, logs the execution time of the function.
            log_level (str): Logging level.
            show_args (bool): If True, logs the arguments passed to the function.

        Returns:
            Callable: A decorator for the given settings.
        """

        def decorator(func: Callable[P_log, T_log]) -> Callable[P_log, T_log]:
            def wrapped_function(*args: P_log.args, **kwargs: P_log.kwargs) -> T_log:
                logger = logging.getLogger(func.__module__)
                level = getattr(logging, log_level.upper(), logging.INFO)
                if show_args:
                    log_args = ', '.join(
                        [repr(a) for a in args] + [f'{k}={v!r}' for k, v in kwargs.items()],
                    )
                enter_message = f'Entering {func.__name__}' + (
                    f' with args: {log_args}' if show_args else ''
                )
                logger.log(level, enter_message)

                start_time = time.perf_counter()
                try:
                    result: T_log = func(*args, **kwargs)
                    if timeit:
                        elapsed_time = time.perf_counter() - start_time
                    exit_message = (
                        f'Exiting {func.__name__}' + f' time elapsed was {elapsed_time:.6f} seconds.'
                    )
                    logger.log(level, exit_message)
                except Exception:
                    logger.exception('Exception in %s', func.__name__)
                    raise
                else:
                    return result

            return wrapped_function

        return decorator


# Example usage
if __name__ == '__main__':
    os.environ['API_KEY_SECRET'] = '12345-abcde-secret-key'  # noqa: S105

    EnhancedLogging.configure_logger(env='development', sensitive_suffix='_SECRET')

    @EnhancedLogging.trace_function(show_args=True, timeit=True)
    def sample_function(x: int, y: int, z: int) -> int:
        """A function to test the log wrapping decorator."""
        x += 1
        logging.info(
            'Test log with secret key: 12345-abcde-secret-key and email: example@example.com',
        )
        return x + y + z

    sample_function(42, 43, z=10)
