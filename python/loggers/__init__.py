"""Logger Utility.

This package provides advanced logging utilities including an enhanced logger with
automatic redaction of sensitive data and an asynchronous file handler for efficient
log writing in I/O-bound applications.

Modules:
    - enhanced_logging: Provides an advanced logger with sensitive data redaction.
    - async_file_handler: Contains the AsyncFileHandler for asynchronous file logging.

The enhanced_logging module offers features like auto-redaction of sensitive patterns,
decorator for function logging, and configurable logging levels. The async_file_handler
module provides a custom logging handler that supports asynchronous log writing, crucial
for non-blocking I/O operations.

Copyright (c) 2024 Neil Schneider

Usage:
    from your_package_name import EnhancedLogging, AsyncFileHandler

    # Example usage of EnhancedLogging
    EnhancedLogging.configure_logger()

    # Example usage of AsyncFileHandler
    logger = logging.getLogger(__name__)
    async_handler = AsyncFileHandler('path/to/logfile.log')
    logger.addHandler(async_handler)

"""

from devops_toolkit.logger.async_file_handler import AsyncFileHandler
from utils.logger.enhanced_logging import EnhancedLogging

__all__ = ['AsyncFileHandler', 'EnhancedLogging']
